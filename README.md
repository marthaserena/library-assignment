# LIBRARY ASSIGNMENT 

Library assignment project 1

## Name
Library Project.

## Description
This is a web based application which is going to be used by students and a librarian. It is going to be used to help them monitor the books borrowed by students.

The Librarian will able to;
-log into the system.
-post books available.
-give penalties to students who don't return books in time.
-view a report showing all books availble, books requested for as well as penaltied for different students.

The student will be able to;
-Log into the system.
-search for books with relation to the title, author, publication date and subject area.
-request for a book.
-set a return date for the book requested.
-receive a notifaction a day before the return date.
-receive penalties if time goes beyond the return date.
